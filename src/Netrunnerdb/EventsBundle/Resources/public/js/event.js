
var lat, lng, address_components, timeZoneId;

function update_address() {
	var address = $('#freelocation').val();
	$.ajax('http://maps.google.com/maps/api/geocode/json', {
		data: {
			address: address
		},
		dataType : 'json',
		success: function(data) {
			if(data.status === "OK") {
				var result = data.results.pop();
				lat = result.geometry.location.lat;
				lng = result.geometry.location.lng;

				request_timezone();
				
				address_components = {
					address: result.formatted_address,
					latitude: lat,
					longitude: lng
				};
				
				request_map();

				for(var i=0; i<result.address_components.length; i++) {
					var address_component = result.address_components[i];
					var type = address_component.types[address_component.types.length > 2 ? 1 : 0];
					address_components[type] = address_component.long_name;
					address_components[type+'_code'] = address_component.short_name;
				}
				address_components.area = address_components.administrative_area_level_1;
				if(!address_components['locality']) {
					if(address_components['sublocality']) {
						address_components['locality'] = address_components['sublocality'];
					}
				}

				$.each(address_components, function (index, value) {
					$('#location-'+index).html(value);
					$('#'+index).val(value);
				});
				
			}
		},
		error: function( jqXHR, textStatus, errorThrown ) {
			console.log('['+moment().format('YYYY-MM-DD HH:mm:ss')+'] Error on '+this.url, textStatus, errorThrown);
		}
	});
}
function request_timezone() {
	$.ajax('https://maps.googleapis.com/maps/api/timezone/json', {
		data: {
			location: lat+","+lng,
			timestamp: moment().unix()
		},
		dataType : 'json',
		success: function(data) {
			if(data.status === "OK") {
				timeZoneId = data.timeZoneId;
				$('#timezone option').each(function (index, element) {
					if($(element).html() === timeZoneId) {
						$(element).prop("selected", "selected");
						$('#timezone-help').html("The value <u>"+timeZoneId+"</u> has been set automatically from the location of your event. Don't change it unless you know what you're doing!");
					} else {
						$(element).removeAttr("selected");
					}
				});
			}
		},
		error: function( jqXHR, textStatus, errorThrown ) {
			console.log('['+moment().format('YYYY-MM-DD HH:mm:ss')+'] Error on '+this.url, textStatus, errorThrown);
		}
	});
}
function request_map() {
	$('#map-canvas').css('height', 500);
	var mapCanvas = document.getElementById('map-canvas');
	var myLatlng = new google.maps.LatLng(lat, lng);
	var mapOptions = {
      center: myLatlng,
      zoom: 13,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
	  draggable: false,
	  keyboardShortcuts: false,
	  mapTypeControl: true,
	  overviewMapControl: false,
	  panControl: false,
	  rotateControl: false,
	  scaleControl: false,
	  scrollwheel: false,
	  streetViewControl: false,
	  zoomControl: true,
	  disableDoubleClickZoom: true
    }
    var map = new google.maps.Map(mapCanvas, mapOptions);
	var marker = new google.maps.Marker({
		map: map,
		position: myLatlng,
		title: address_components.address
	});
	
}
